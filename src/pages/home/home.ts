import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, NavController, Platform } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {



  /**
   * @name hasData
   * @type {Boolean}
   * @public
   * @description     Bandera utilizada en la plantilla para controlar la visualización de los registros de la base de datos
   */
  public hasData: boolean = false;



  /**
   * @name technologies
   * @type {object}
   * @public
   * @description     Almacena todos los registros de la tabla de la base de datos recuperados
   */
  public technologies: any;



  /**
   * @name dataImported
   * @type {Boolean}
   * @public
   * @description     Marcador utilizado para determinar si los datos se han importado a la base de datos SQLite
   */
  public dataImported: boolean = false;



  /**
   * @name _SQL_NAME
   * @type {String}
   * @private
   * @description     Nombre del archivo SQL
   */
  private _SQL_NAME: string = 'technologies.sql';



  /**
   * @name _SQL_URI
   * @type {String}
   * @private
   * @description     Ubicación del archivo SQL
   */
  private _SQL_URI: string = encodeURI("http://contalentosas.com/technologies.sql");



  /**
   * @name _JSON_NAME
   * @type {String}
   * @private
   * @description     Nombre del archivo JSON
   */
  private _JSON_NAME: string = 'technologies.json';



  /**
   * @name _JSON_URI
   * @type {String}
   * @private
   * @description     Ubicación del archivo JSON
   */
  private _JSON_URI: string = encodeURI("http://contalentosas.com/technologies.json");




  /**
   * @name _REMOTE_URI
   * @type {String}
   * @private
   * @description     La dirección del script PHP remoto
   */
  private _REMOTE_URI: string = "http://REMOTE-URI-HERE/parse-data.php";




  constructor(public navCtrl: NavController,
    private _ALERT: AlertController,
    private _HTTP: HttpClient,
    private _DB: DatabaseProvider,
    private _PLAT: Platform) { }




  /**
   * @public
   * @method ionViewDidLoad
   * @description         Verifique si los datos existen en la carga de vista de plantilla
   * @return {none}
   */
  ionViewDidLoad(): void {
    this._PLAT
      .ready()
      .then(() => {
        setTimeout(() => {
          this._DB
            .dataExistsCheck('technologies')
            .then((data) => {
              this.loadRecords();
            })
            .catch((error) => {
              console.dir(error);
            });
        }, 1500);
      });
  }




  /**
   * @public
   * @method loadRecords
   * @description         Recupere registros de la base de datos y, en caso de éxito, configure el indicador hasData en true
   * @return {none}
   */
  loadRecords(): void {
    this._DB
      .retrieveAllRecords()
      .then((data: any) => {
        this.hasData = true;
        this.technologies = data;
      })
      .catch((error: any) => {
        console.dir(error);
      });
  }




  /**
   * @public
   * @method import
   * @description         Mostrar una ventana de alerta que permite al usuario seleccionar su tipo de importación
   * @return {none}
   */
  import(): void {
    this._DB
      .dataExistsCheck('technologies')
      .then((data: any) => {
        // Si tenemos datos existentes, limpie la base de datos
        if (data > 0) {
          this._DB
            .clear()
            .then((data) => {
              // Si la base de datos se borró exitosamente de tablas / datos, llame al método importAlert
              this.hasData = false;
              this.importAlert();
            })
            .catch((error) => {
              console.dir(error);
            });
        }
        // Si no tenemos datos existentes solo llame al método importAlert
        else {
          this.importAlert();
        }
      })
      .catch((error) => {
        this.importAlert();
      });
  }




  /**
   * @public
   * @method retrieveSQLFile
   * @description         Recuperar archivo SQL remoto utilizando el método de obtención de HttpClient angular
   * @return {none}
   */
  retrieveSQLFile(): void {
    this._HTTP
      .get(this._SQL_URI, { responseType: 'text' })
      .subscribe((data) => {
        this.importSQL(data);
      },
        (error) => {
          console.dir(error);
        });
  }




  /**
   * @public
   * @method retrieveJSONFile
   * @description         Recuperar archivo JSON remoto utilizando el método de obtención de HttpClient angular
   * @return {none}
   */
  retrieveJSONFile(): void {
    this._HTTP
      .get(this._JSON_URI)
      .subscribe((data) => {
        this.importJSON(data);
      },
        (error) => {
          console.dir(error);
        });
  }




  /**
   * @public
   * @method importSQL
   * @param sqlFile     {any}      Los datos del archivo SQL que se importará
   * @description         Importar archivo SQL
   * @return {none}
   */
  importSQL(sqlFile: any): void {
    this._DB
      .importSQL(sqlFile)
      .then((res) => {
        this.dataImported = true;
        this.loadRecords();
      })
      .catch((error) => {
        console.dir(error);
      });
  }




  /**
   * @public
   * @method importJSON
   * @param jsonFile     {any}      Los datos del archivo JSON que se importará
   * @description         Importar archivo JSON
   * @return {none}
   */
  importJSON(jsonFile: any): void {
    this._DB
      .importJSON(jsonFile)
      .then((res) => {
        this.dataImported = true;
        this.loadRecords();
      })
      .catch((error) => {
        console.dir(error);
      });
  }




  /**
   * @public
   * @method importAlert
   * @description         Mostrar una ventana de alerta que permite al usuario seleccionar su tipo de importación de datos: SQL o JSON
   * @return {none}
   */
  importAlert(): void {
    let alert: any = this._ALERT.create({
      title: 'Import data',
      subTitle: 'Please select which import option you prefer',
      buttons: [
        {
          text: 'JSON',
          handler: () => {
            this.retrieveJSONFile();
          }
        },
        {
          text: 'SQL',
          handler: () => {
            this.retrieveSQLFile();
          }
        }
      ]
    });
    alert.present();
  }




  /**
   * @public
   * @method exportAlert
   * @description         Mostrar una ventana de alerta que permite al usuario seleccionar su tipo de exportación de datos: actualmente solo SQL
   * @return {none}
   */
  exportAlert(): void {
    let alert: any = this._ALERT.create({
      title: 'Export data',
      subTitle: 'Please select which export option you prefer',
      buttons: [
        {
          text: 'SQL',
          handler: () => {
            this.exportToSQL();
          }
        }
      ]
    });
    alert.present();
  }




  /**
   * @public
   * @method exportToSQL
   * @description         Maneja la exportación de datos SQL utilizando el servicio DatabaseProvider
   * @return {none}
   */
  exportToSQL(): void {
    this._DB
      .exportAsSQL()
      .then((res) => {
        let fileName: any = Date.now() + '.sql';
        this.parseAndUploadSQL(fileName, res);
      })
      .catch((error) => {
        console.dir(error);
      });
  }




  /**
   * @public
   * @method parseAndUploadSQL
   * @param fileName     {String}      El nombre de archivo para los datos SQL exportados
   * @param fileName     {String}      Los datos SQL exportados
   * @description        Publica los datos SQL exportados en el script PHP remoto utilizando el módulo HttpClient de Angular
   * @return {none}
   */
  parseAndUploadSQL(fileName: string, sqlData: any) {
    let headers: any = new HttpHeaders({ 'Content-Type': 'application/octet-stream' }),
      options: any = { "name": fileName, "data": sqlData };

    this._HTTP
      .post(this._REMOTE_URI, JSON.stringify(options), headers)
      .subscribe((res: any) => {
        console.dir(res);
      },
        (error: any) => {
          console.dir(error);
        });
  }

}
