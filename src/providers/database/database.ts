import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { SQLitePorter } from '@ionic-native/sqlite-porter';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {



  /**
   * @name _DB
   * @type {object}
   * @private
   * @description     Define un objeto para manejar la interfaz con el
                       Plugin de SQLite
   */
  private _DB: SQLiteObject;




  /**
   * @name _DB_NAME
   * @type {object}
   * @private
   * @description     Define el nombre de la base de datos SQLite que se creará
   */
  private _DB_NAME: string = "ionic.db";




  constructor(public http: HttpClient,
    private _PLAT: Platform,
    private _SQL: SQLite,
    private _PORTER: SQLitePorter) { }




  /**
   * @public
   * @method init
   * @description          Crea la base de datos SQLite
   * @return {none}
   */
  init(): void {
    // Definir la base de datos SQLite de la aplicación
    this._SQL.create({
      name: this._DB_NAME,
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        // Asociar el objeto del manejador de base de datos con la propiedad privada _DB
        this._DB = db;
      })
      .catch((e) => {
        console.log(e);
      });
  }




  /**
   * @public
   * @method dataExistsCheck
   * @param tableName    {String}          Nombre de la tabla que queremos verificar para los datos
   * @description          Comprueba que los datos existen dentro de la tabla SQLite especificada
   * @return {Promise}
   */
  dataExistsCheck(tableName: string): Promise {
    return new Promise((resolve, reject) => {
      this._DB.executeSql('SELECT count(*) AS numRows FROM ' + tableName, {})
        .then((data: any) => {
          var numRows = data.rows.item(0).numRows;
          resolve(numRows);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }




  /**
   * @public
   * @method retrieveAllRecords
   * @description          Recupera todos los registros almacenados de la tabla SQLite de tecnologías
   * @return {Promise}
   */
  retrieveAllRecords(): Promise {
    return new Promise((resolve, reject) => {

      this._DB.executeSql('SELECT id, name, description FROM technologies', {})
        .then((data: any) => {
          let items: any = [];
          if (data.rows.length > 0) {
            var k;

            // iterar a través de registros devueltos y empujar como objetos anidados en
            // el array de elementos
            for (k = 0; k < data.rows.length; k++) {
              items.push(
                {
                  id: data.rows.item(k).id,
                  name: data.rows.item(k).name,
                  description: data.rows.item(k).description
                });
            }
          }
          resolve(items);
        })
        .catch((error: any) => {
          reject(error);
        });

    });
  }




  /**
   * @public
   * @method importSQL
   * @param sql    {String}          Los datos SQL a importar
   * @description          Importa los datos SQL suministrados a la base de datos de la aplicación
   * @return {Promise}
   */
  importSQL(sql: any): Promise {
    return new Promise((resolve, reject) => {
      this._PORTER.importSqlToDb(this._DB, sql)
        .then((data) => {
          resolve(data);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }




  /**
   * @public
   * @method exportAsSQL
   * @description          Exporta datos SQL de la base de datos de la aplicación
   * @return {Promise}
   */
  exportAsSQL(): Promise {
    return new Promise((resolve, reject) => {
      this._PORTER
        .exportDbToSql(this._DB)
        .then((data) => {
          resolve(data);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }




  /**
   * @public
   * @method importJSON
   * @param json    {String}          Los datos JSON a importar
   * @description          Importa los datos JSON suministrados a la base de datos de la aplicación
   * @return {Promise}
   */
  importJSON(json: any): Promise {
    return new Promise((resolve, reject) => {
      this._PORTER
        .importJsonToDb(this._DB, json)
        .then((data) => {
          resolve(data);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }




  /**
   * @public
   * @method clear
   * @description          Elimina todas las tablas / datos de la base de datos de la aplicación
   * @return {Promise}
   */
  clear(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._PORTER
        .wipeDb(this._DB)
        .then((data) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

}
